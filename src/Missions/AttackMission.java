package Missions;

import AerialVehicles.IAerialVehicle;
import Capabilities.IAttackCapability;
import Entities.Coordinates;

public class AttackMission extends Mission {

    private String target;

    public AttackMission(Coordinates destination, String pilotName, IAerialVehicle aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
            if (this.getAerialVehicle().HasCapability(this.getAerialVehicle().getCapability())) {
                String pilotName = this.getPilotName();
                String platformName = this.getAerialVehicle().getClass().getSimpleName();
                String weapon = ((IAttackCapability) this.getAerialVehicle().getCapability()).getMissileType().label;
                int count = ((IAttackCapability) this.getAerialVehicle().getCapability()).getMissileCount();
                System.out.println(pilotName + ": " + platformName + " Attacking " + target + " with: " + weapon + "X" + count);
            } else {
                String msg = "Oh man...";
                throw new AerialVehicleNotCompatibleException(msg);
            }
    }
}
