package Missions;

import AerialVehicles.IAerialVehicle;
import Capabilities.IAttackCapability;
import Capabilities.IBdACapabiliy;
import Entities.Coordinates;

public class BdAMission extends Mission{

    private String objective;

    public BdAMission(Coordinates destination, String pilotName, IAerialVehicle aerialVehicle, String region) {
        super(destination, pilotName, aerialVehicle);
        this.objective = region;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if (this.getAerialVehicle().HasCapability(this.getAerialVehicle().getCapability())) {
            String pilotName = this.getPilotName();
            String platformName = this.getAerialVehicle().getClass().getSimpleName();
            String cameraType = ((IBdACapabiliy) this.getAerialVehicle().getCapability()).getCameraType().label;
            System.out.println(pilotName + ": " + platformName + " taking pictures of " + this.objective + " with: " + cameraType + " camera");
        } else {
            String msg = "Oh man...";
            throw new AerialVehicleNotCompatibleException(msg);
        }
    }
}
