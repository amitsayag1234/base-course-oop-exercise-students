package Missions;

import AerialVehicles.IAerialVehicle;
import Capabilities.IAttackCapability;
import Capabilities.IIntelCapability;
import Capabilities.IntelCapability;
import Entities.Coordinates;

public class IntelMission extends Mission{

    private String region;

    public IntelMission(Coordinates destination, String pilotName, IAerialVehicle aerialVehicle, String objective) {
        super(destination, pilotName, aerialVehicle);
        this.region = objective;
    }



    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(this.getAerialVehicle().HasCapability(this.getAerialVehicle().getCapability())) {
            String pilotName = this.getPilotName();
            String platformName = this.getAerialVehicle().getClass().getSimpleName();
            String sensorType = ((IIntelCapability) this.getAerialVehicle().getCapability()).getSensorType().label;
            System.out.println(pilotName + ": " + platformName + " Collecting Data in " + this.region + " with: " + sensorType);
        }else{
            String msg = "Oh man, I Can't perform an intelligence mission.";
            throw new AerialVehicleNotCompatibleException(msg);
        }
    }

}
