package Missions;

public interface IMission {
    void executeMission() throws AerialVehicleNotCompatibleException;
    void begin();
    void cancel();
    void finish();
}
