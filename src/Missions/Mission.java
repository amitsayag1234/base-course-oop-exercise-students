package Missions;

import AerialVehicles.IAerialVehicle;
import Entities.Coordinates;

public abstract class Mission implements IMission{

    private Coordinates destination;
    private String pilotName;
    private IAerialVehicle aerialVehicle;

    public Mission(Coordinates destination, String pilotName, IAerialVehicle aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    @Override
    public void begin() {
        // print
        this.aerialVehicle.flyTo(this.destination);
    }

    protected Coordinates getDestination() {
        return destination;
    }

    protected String getPilotName() {
        return pilotName;
    }

    protected IAerialVehicle getAerialVehicle() {
        return aerialVehicle;
    }

    @Override
    public void cancel() {
        //print
        this.aerialVehicle.land();
    }

    @Override
    public void finish() {
        try {
            this.executeMission();
            this.aerialVehicle.land();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

    }
}
