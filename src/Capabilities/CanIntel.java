package Capabilities;

public interface CanIntel {
    int getMissileCount();
    MissileType getMissileType();
}
