package Capabilities;

public class AttackCapability extends Capability implements IAttackCapability {
    private int missileCount;
    private MissileType missileType;

    public AttackCapability(int missileCount, MissileType missileType) {
        this.missileCount = missileCount;
        this.missileType = missileType;
    }

    public int getMissileCount() {
        return this.missileCount;
    }

    public void setMissileCount(int missileCount) {
        this.missileCount = missileCount;
    }

    public MissileType getMissileType() {
        return this.missileType;
    }

    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}
