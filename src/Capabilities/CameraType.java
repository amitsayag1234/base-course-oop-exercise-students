package Capabilities;

public enum CameraType {
    REGULAR("Regular"), THERMAL("Thermal"), NIGHTVISION("NightVision");

    public final String label;

    CameraType(String label) {
        this.label = label;
    }
}
