package Capabilities;

public interface IAttackCapability {
    int getMissileCount();
    void setMissileCount(int missileCount);
    MissileType getMissileType();
    void setMissileType(MissileType missileType);
}
