package Capabilities;

public class BdACapability extends Capability implements IBdACapabiliy{

    public BdACapability(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    private CameraType cameraType;

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }
}
