package Capabilities;

public class IntelCapability extends Capability implements  IIntelCapability{
    private SensorType sensorType;

    public IntelCapability(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }
}
