package Capabilities;

public enum SensorType {
    INFRARED("InfraRed"), ELINT("Elint");

    public final String label;

    SensorType(String label) {
        this.label = label;
    }
}
