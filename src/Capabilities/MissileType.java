package Capabilities;

public enum MissileType {
    PYTHON("Python"), AMRAM("Amram"), SPICE250("Spice250");

    public final String label;

    MissileType(String label) {
        this.label = label;
    }
}
