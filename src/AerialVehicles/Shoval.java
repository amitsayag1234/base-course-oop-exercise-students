package AerialVehicles;


import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IBdACapabiliy;
import Capabilities.IIntelCapability;
import Entities.Coordinates;

public class Shoval extends Haron{
    public Shoval(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean HasCapability(Capability capability) {
        return (capability instanceof IAttackCapability) ||
                (capability instanceof IIntelCapability) ||
                (capability instanceof IBdACapabiliy);
    }
}

