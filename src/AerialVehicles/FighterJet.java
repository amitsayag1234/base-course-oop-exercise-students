package AerialVehicles;

import Capabilities.AttackCapability;
import Capabilities.Capability;
import Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle {
    protected AttackCapability attackCapability;

    public FighterJet(Coordinates defaultBase, Capability capability) {

        super(defaultBase, capability);
    }

    @Override
    protected double getMaxHoursBeforeNextRepair() {
        return 250;
    }
}
