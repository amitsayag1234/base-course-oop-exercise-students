package AerialVehicles;

import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IBdACapabiliy;
import Capabilities.IIntelCapability;
import Entities.Coordinates;

public class Eitan extends Haron{

    public Eitan(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean HasCapability(Capability capability) {
        return (capability instanceof IAttackCapability) ||
                (capability instanceof IIntelCapability);
    }
}