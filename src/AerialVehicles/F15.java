package AerialVehicles;

import Capabilities.*;
import Entities.Coordinates;

public class F15 extends FighterJet{

    public F15(Coordinates defaultBase, Capability capability) {

        super(defaultBase, capability);
    }

    @Override
    public boolean HasCapability(Capability capability) {
        return (capability instanceof IAttackCapability) ||
                (capability instanceof IIntelCapability);
    }
}
