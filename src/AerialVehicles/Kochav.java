package AerialVehicles;

import Capabilities.*;
import Entities.Coordinates;

public class Kochav extends Hermes{
    //private IAttackCapability attackCapability;
    private Capability capability;

    public Kochav(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean HasCapability(Capability capability) {
        return (capability instanceof IAttackCapability) ||
                (capability instanceof IIntelCapability) ||
                (capability instanceof IBdACapabiliy);
    }
}