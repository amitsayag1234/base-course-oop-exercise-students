package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public abstract class Hermes extends UAV{
    public Hermes(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    protected double getMaxHoursBeforeNextRepair() {
        return 100;
    }
}
