package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public abstract class AerialVehicle implements IAerialVehicle{

    protected double hoursSinceLastRepair;
    protected ReadinessStatus readinessStatus;
    protected Coordinates defaultBase;

    @Override
    public Capability getCapability() {
        return capability;
    }

    protected Capability capability;

    public AerialVehicle(Coordinates defaultBase, Capability capability) {
        this.hoursSinceLastRepair = 0;
        this.readinessStatus = ReadinessStatus.READY;
        this.defaultBase = defaultBase;
        this.capability = capability;
    }

    protected abstract double getMaxHoursBeforeNextRepair();

    @Override
    public void flyTo(Coordinates destination) {
        if(this.readinessStatus.equals(ReadinessStatus.READY)) {
            System.out.println("Flying to " + destination); //כשרושמים + בין סטרינג  לאובייקט, הוא עושה toSting
            this.readinessStatus = ReadinessStatus.IN_FLIGHT;
        }else{
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    @Override
    public void land(Coordinates destination) {
        System.out.println("Landing on " + destination);
        check();
    }

    @Override
    public void land() {
        this.land(this.defaultBase);
    }

    @Override
    public void check() {
        double maxHoursBeforeNextRepair = this.getMaxHoursBeforeNextRepair();
        if(hoursSinceLastRepair >= maxHoursBeforeNextRepair){
            readinessStatus = ReadinessStatus.NOT_READY;
            this.repair();
        }else{
            readinessStatus = ReadinessStatus.READY;
        }
    }

    @Override
    public void repair() {
        this.hoursSinceLastRepair = 0;
    }
}