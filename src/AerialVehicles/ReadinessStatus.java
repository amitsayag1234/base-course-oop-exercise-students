package AerialVehicles;

public enum ReadinessStatus {
    READY, NOT_READY, IN_FLIGHT;
}
