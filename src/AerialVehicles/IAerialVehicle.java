package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public interface IAerialVehicle {
    boolean HasCapability(Capability capability);
    Capability getCapability();
    void flyTo(Coordinates destination);
    void land(Coordinates destination);
    void check();
    void repair();
    void land();
}