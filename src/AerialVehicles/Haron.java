package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public abstract class Haron extends UAV{


    public Haron(Coordinates defaultBase, Capability capability) {

        super(defaultBase, capability);
    }

    @Override
    protected double getMaxHoursBeforeNextRepair() {
        return 150;
    }
}
