package AerialVehicles;


import Capabilities.Capability;
import Capabilities.IAttackCapability;
import Capabilities.IBdACapabiliy;
import Capabilities.IIntelCapability;
import Entities.Coordinates;

public class Zik extends Hermes{
    public Zik(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public boolean HasCapability(Capability capability) {
        return (capability instanceof IIntelCapability) ||
                (capability instanceof IBdACapabiliy);
    }
}