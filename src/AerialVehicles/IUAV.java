package AerialVehicles;

import Entities.Coordinates;

public interface IUAV extends IAerialVehicle{
    void hoverOverLocation(Coordinates destination);
}
