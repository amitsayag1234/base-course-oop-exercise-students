package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;

public abstract class UAV extends AerialVehicle implements IUAV{
    public UAV(Coordinates defaultBase, Capability capability) {
        super(defaultBase, capability);
    }

    @Override
    public void hoverOverLocation(Coordinates destination) {
        this.readinessStatus = ReadinessStatus.IN_FLIGHT;
        System.out.println("Hovering over " + destination);
    }
}
