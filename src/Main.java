import AerialVehicles.*;
import Capabilities.*;
import Entities.Coordinates;
import Missions.*;

public class Main {

    public static void main(String[] args) {

        String pilotName, region, objective, target;
        Coordinates defaultBase, destCord;
        UAV myUAV;
        FighterJet myFighterJet;

        // part 1 - Intelligence
        defaultBase = new Coordinates(31.827604665263365, 34.81739714569337);
        destCord = new Coordinates(33.2033427805222, 44.5176910795946);
        pilotName = "Dror zalicha";
        region = "Iraq";
        myUAV = new Kochav(defaultBase, new IntelCapability(SensorType.INFRARED));
        IMission mission1 = new IntelMission(destCord, pilotName, myUAV, region);
        mission1.begin();            // fly to
        myUAV.hoverOverLocation(destCord); // hover over
        mission1.finish();           // Intelligence + landing

        System.out.println("\n");

        // part 2 - attack
        pilotName = "Ilan Ramon";
        target = "Tuwaitha Nuclear Research Center";
        myFighterJet = new F16(defaultBase, new AttackCapability(4, MissileType.SPICE250));
        IMission mission2 = new AttackMission(destCord, pilotName, myFighterJet, target);
        mission2.begin();            // fly to
        mission2.finish();           // Attack + landing

        System.out.println("\n");

        // part 3 -  BdA
        pilotName = "Ilan Ramon";
        objective = "Tuwaitha Nuclear Research Center";
        myFighterJet = new F16(defaultBase, new BdACapability(CameraType.THERMAL));
        IMission mission3 = new BdAMission(destCord, pilotName, myFighterJet, objective);
        mission3.begin();            // fly to
        mission3.finish();           // BdA + landing
    }
}