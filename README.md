In this file you can read about the various scenarios that you need to implement.

Operation "Opera":
    Main goal: gathering intelligence, attack and preform BDA on the Tuwaitha Nuclear Research Center (Iraq)

    
    Inelligence mission:
        Details:
            - coordinates: 33.2033427805222, 44.5176910795946
            - pilot name: Dror zalicha
            - airplane capabilities: intelligence
            - region: Iraq
        Airplane:
            - UAV (not a fighter jet)
            - home base coordinates: 31.827604665263365, 34.81739714569337
            attack:
                - 4 python missiles 
            intelligence:
                - InfraRed sensor
        Process:
            1- fly to mission coordinates
            2- hover over mission coordinates
            3- activate intelligence ability
            4- land at home base coordinates

    
    Attack mission:
        Details:
            - coordinates: 33.2033427805222, 44.5176910795946
            - pilot name: Ze'ev Raz
            - airplane capabilities: attack
            - target: Tuwaitha Nuclear Research Center
        Airplane:
            - Fighter jet
            - home base coordinates: 31.827604665263365, 34.81739714569337
            attack:
                - Spice 250 missiles
        Process:
            1- fly to mission coordinates
            2- activate attack ability
            3- land at home base coordinates

    Bda mission:
        Details:
            - coordinates: 33.2033427805222, 44.5176910795946
            - pilot name: Ilan Ramon
            - airplane capabilities: Bda
            - objective: Tuwaitha Nuclear Research Center
        Airplane:
            - Fighter jet
            - home base coordinates: 31.827604665263365, 34.81739714569337
            bda:
                - Thermal Camera
        Process:
            1- fly to mission coordinates
            2- activate bda ability
            3- land at home base coordinates
